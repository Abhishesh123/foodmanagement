# Generated by Django 2.2.3 on 2019-08-12 06:45

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0003_foodprice'),
    ]

    operations = [
        migrations.CreateModel(
            name='FakeClass',
            fields=[
                ('foodprice_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='products.FoodPrice')),
                ('serve_pizza', models.CharField(max_length=200)),
            ],
            bases=('products.foodprice',),
        ),
    ]
