from django.db import models


class Category(models.Model):
    cat_name = models.CharField(max_length=200)
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.cat_name


class Food(models.Model):
    categoryid = models.ForeignKey(Category, on_delete=models.CASCADE, related_name='foodcategory')
    name = models.CharField(max_length=200)
    price = models.IntegerField()
    img = models.ImageField(upload_to='products/image')
    slug = models.SlugField()
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "{}- ({})".format(self.name, self.categoryid.cat_name)


class FoodPrice(models.Model):
    food = models.ForeignKey(Food, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    img = models.ImageField(upload_to='foodpartner')
    price = models.SmallIntegerField()

    def __str__(self):
        return "{}({})".format(self.name, self.food.categoryid.cat_name)


class FakeClass(FoodPrice):
    serve_pizza = models.CharField(max_length=200)

    def __str__(self):
        return self.serve_pizza
