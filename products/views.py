from django.shortcuts import render
from django.views import generic
from .models import Food, FoodPrice


class Home(generic.TemplateView):
    model = Food
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['thalis'] = Food.objects.filter(categoryid__cat_name__icontains='thali')
        context['pizzas'] = Food.objects.filter(categoryid__cat_name='pizza')
        context['chineses'] = Food.objects.filter(categoryid__cat_name='Chinese')
        return context


class ProductDetails(generic.TemplateView):
    model = Food
    template_name = 'product-compare.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        the_slug = kwargs.pop('the_slug')
        context['products'] =FoodPrice.objects.filter(food__slug__exact=the_slug)
        p=Food.objects.in_bulk([1,5])
        print(p)
        return context
class ProductListing(generic.TemplateView):
    model=Food
    template_name = 'product-list.html'
    def get_context_data(self, **kwargs):
        context=super().get_context_data()
        the_slug = kwargs.pop('the_slug')
        context['products'] = Food.objects.filter(categoryid__cat_name=the_slug)
        return context