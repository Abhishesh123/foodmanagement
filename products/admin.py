from django.contrib import admin
from .models import Food, Category, FoodPrice,FakeClass

admin.site.register(Food)
admin.site.register(Category)
admin.site.register(FoodPrice)
admin.site.register(FakeClass)
