from foodmanagement import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from .views import *
from django.urls import path
from django.conf.urls.static import static

urlpatterns = [
                  path('', Home.as_view(), name='home'),
                  path('details/<slug:the_slug>', ProductDetails.as_view(), name='productdetails'),
                  path('product-details/<slug:the_slug>', ProductListing.as_view(), name='productlisting'),
              ] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
